﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrintServerApp.Services;
using System.IO;
using ZeroconfService;
using System.Diagnostics;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Drawing.Printing;
using System.DirectoryServices;

namespace PrintServerApp
{
    public partial class PrintManager : Form
    {
        #region Properties 

        UploadWatcher watcher = new UploadWatcher();
        
        NetService publishService = null;

        public  string SharedFolderForImages { get; set; }
        public  string ShareName { get; set; }

        private bool isInitialLoad { get; set; }
        public bool IsLandscape { get; set; }

        public int LeftMargin { get; set; }
        public int RightMargin { get; set; }
        public int TopMargin { get; set; }
        public int BottomMargin { get; set; }

        public string ReprintImagePath { get; set; }

        public string PrintedImagesFolder { get; set; }

        public List<ShareMaker.SharedFileModel> PreviousEventList { get; set; }

        private ShareMaker.SharedFileModel m_CurrentEvent;
        public ShareMaker.SharedFileModel CurrentEvent {
            get { return m_CurrentEvent; }
            set {
                m_CurrentEvent = value;
                lblImageFolder.Text = m_CurrentEvent.ShareName;
                ShareName = m_CurrentEvent.ShareName;
                SharedFolderForImages = m_CurrentEvent.FilePath;
                PrintedImagesFolder = string.Format(@"{0}\{1}", SharedFolderForImages, @"\PrintedImages");
            }
        }

        public enum ViewMode
        {
            //
            // Summary:
            //     Each item appears as a full-sized icon with a label below it.
            LargeIcon = 0,
            //
            // Summary:
            //     Each item appears on a separate line with further information about each item
            //     arranged in columns. The left-most column contains a small icon and label, and
            //     subsequent columns contain sub items as specified by the application. A column
            //     displays a header which can display a caption for the column. The user can resize
            //     each column at run time.
            //   Details = 1,
            //
            // Summary:
            //     Each item appears as a small icon with a label to its right.
            //   SmallIcon = 2,
            //
            // Summary:
            //     Each item appears as a small icon with a label to its right. Items are arranged
            //     in columns with no column headers.
            List = 3,
            //
            // Summary:
            //     Each item appears as a full-sized icon with the item label and subitem information
            //     to the right of it. The subitem information that appears is specified by the
            //     application. This view is available only on Windows XP and the Windows Server
            //     2003 family. On earlier operating systems, this value is ignored and the System.Windows.Forms.ListView
            //     control displays in the System.Windows.Forms.View.LargeIcon view.
            Tile = 4
        }

        #endregion

        public PrintManager()
        {
            InitializeComponent();

        }


        #region GUI EVENTS

        void publishService_DidPublishService(NetService service)
        {

            string svcDescription = UTF8Encoding.ASCII.GetString(service.TXTRecordData);
            //Console.WriteLine("Published Bonjour Service: domain({0}) type({1}) name({2}) pathToService({3})", service.Domain, service.Type, service.Name, svcDescription);


        }

        void publishService_DidNotPublishService(NetService service, DNSServiceException exception)
        {
            MessageBox.Show(String.Format("A DNSServiceException occured: {0}", exception.Message), "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);





        }



        private void rdoPortrait_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoPortrait.Checked)
            {
                IsLandscape = false;
            }
            else
            {
                IsLandscape = true;
            }
           
        }

        private void rdoLandscape_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoLandscape.Checked)
            {
                IsLandscape = true;
            }
            else
            {
                IsLandscape = false;
            }

        }



        private void DefaultCopyQuantityKeypress(object sender, KeyPressEventArgs e)
        {
            // 48 is zero
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) || e.KeyChar == 48)
            {
                e.Handled = true;
            }
          

        }

        private void CopyQuantityDone(object sender, EventArgs e)
        {
            string boxValue = txtDefaultToPrint.Text;
            if (boxValue == "")
            {
                MessageBox.Show("The Default Quantity cannot be empty and must be greater than zero.", "Cannot be empty.", MessageBoxButtons.OK);
                txtDefaultToPrint.Focus();
            }
          

        }


        private void TopChanged(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
         
        }



        private void TopMarginDone(object sender, EventArgs e)
        {
            string boxValue = txtTop.Text;
            if (boxValue == "")
            {
                MessageBox.Show("The TOP margin cannot be empty.", "Cannot be empty.", MessageBoxButtons.OK);
                txtTop.Focus();
            }

           

        }

        private void LeftMarginDone(object sender, EventArgs e)
        {
            string boxValue = txtLeft.Text;
            if (boxValue == "")
            {
                MessageBox.Show("The LEFT margin cannot be empty.", "Cannot be empty.", MessageBoxButtons.OK);
                txtLeft.Focus();
            }

           
        }

        private void BottomMarginDone(object sender, EventArgs e)
        {

            string boxValue = txtBottom.Text;
            if (boxValue == "")
            {
                MessageBox.Show("The Bottom margin cannot be empty.", "Cannot be empty.", MessageBoxButtons.OK);
                txtBottom.Focus();
            }

        }

        private void RightMarginDone(object sender, EventArgs e)
        {
            string boxValue = txtRight.Text;
            if (boxValue == "")
            {
                MessageBox.Show("The Right margin cannot be empty.", "Cannot be empty.", MessageBoxButtons.OK);
                txtRight.Focus();
            }

           

        }


        private void CopiesDone(object sender, EventArgs e)
        {
            string boxValue = txtCopies.Text;
            if (boxValue == "")
            {
                MessageBox.Show("The Copies value cannot be empty.", "Cannot be empty.", MessageBoxButtons.OK);
                txtCopies.Focus();
            }

        }



        private void CopiesKeypress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
          
        }

        private void LeftMarginChanged(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
          
        }

        private void BottomMarginChanged(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
           
        }

        private void RightMarginChanged(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
          
        }





        private void cboListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            listView1.View = (View)cboListView.SelectedItem;
        }




        private void btnBrowse_Click(object sender, EventArgs e)
        {

            LoadImagesFromSelectedFolder();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count <= 0)
            {
                return;
            }
            int selectedIndex = listView1.SelectedIndices[0];
            if (selectedIndex >= 0)
            {
                String imagePath = listView1.Items[selectedIndex].Tag.ToString();

                ReprintImagePath = imagePath;

            }
        }


        private void AddNewEvent()
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            DialogResult result;

            result = dlg.ShowDialog();
            if (result == DialogResult.OK && (!isInValidShareName(dlg.SelectedPath)))
            {
                // first create the new shares.. then reload the system.
                
                ShareMaker.SharedFileModel fm = new ShareMaker.SharedFileModel();
                fm.FilePath = dlg.SelectedPath;
                fm.ShareName  = GetValidShareFolder(dlg.SelectedPath);
                
                //set the current event because the create shares uses the sharename and other properties.
                CurrentEvent = fm;

                //now you can create the shares and reload the system
                CreateShares();

                UpdateSelectedEvent(dlg.SelectedPath);



            }
            else
            {
                MessageBox.Show("Invalid folder name.", "Folder name can only include letters, numbers, blank space, dashes(-) and underscores(_).");
            }

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            short numberOfCopies = short.Parse(txtCopies.Text);
            PrintingService printingService = new PrintingService();
            
            printingService.PrintTheImage(ReprintImagePath, IsLandscape, numberOfCopies);
        }

       



        private void browseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewEvent();
        }

        private void browseToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Add code for open existing event?");
        }




        #endregion



        #region Page Functions

        private static string GetValidShareFolder(string theShareName)
        {
            string FolderName = Path.GetFileName(theShareName);
            string pattern = @"\W";

            return Regex.Replace(FolderName, pattern, "");

        }


        private static bool isInValidShareName(string theShareName)
        {
            string pattern = "@W";
            Regex rg = new Regex(pattern);
            return rg.Match(theShareName).Success;
        }


        
        private void PrintManager_Load(object sender, EventArgs e)
        {


            bool IsAccountCreated = Properties.Settings.Default.IsAccountCreated;
            string usrName = Properties.Settings.Default.PrintServerAccount;
            string passwrd = Properties.Settings.Default.PrintServerPassword;
            

            if (IsAccountCreated == false)
            {


                IsAccountCreated = UserAccountMgr.CreateNewUser(usrName, passwrd);
                if (IsAccountCreated == true)
                {
                    
                }
                else
                {
                    MessageBox.Show("Unable to create Account for Sharing files.", "Unable to create account for saving images.", MessageBoxButtons.OK);
                   // PrintManager.ActiveForm.Close();
                }
                
            }

            Properties.Settings.Default.IsAccountCreated = IsAccountCreated;
            txtIsAccountCreated.Text = IsAccountCreated.ToString();

            isInitialLoad = true;

            LoadDefaultValuesForService();


            //load view selector for Images.
            foreach (ViewMode val in Enum.GetValues(typeof(ViewMode)))
            {
                cboListView.Items.Add(val);
            }

            cboListView.SelectedIndex = 0;




            // create shares only if the last event was found. //Otherwise throw an error!
            if (ShareName != null && ShareName != "")
            {
                CreateShares();
                DoPublish();
                LoadImagesFromSelectedFolder();
                watcher.ImageUploadedEvent += ImageUploadHandlerForListview;
            }
            else
            {
                MessageBox.Show("Please select a folder for saving images from the dropdown.", "FOLDER not selected.", MessageBoxButtons.OK);
            }

            isInitialLoad = false;
        }

        


        private void CreateShares()
        {

            ShareMaker shareMaker = new ShareMaker();

            shareMaker.CreateSharedFolder(SharedFolderForImages, ShareName, Properties.Settings.Default.DefaultServiceName);

            
            watcher.Watch(SharedFolderForImages);

        }




        private void DoPublish()
        {
            try
            {
                Debug.WriteLine(String.Format("Bonjour Version: {0}", NetService.DaemonVersion));
            }
            catch (Exception ex)
            {
                String message = ex is DNSServiceException ? "Bonjour is not installed!" : ex.Message;
                MessageBox.Show(message, "Critical Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Exit();
            }




            String domain = "";
            String type = Properties.Settings.Default.DefaultServiceType;
            String serviceShareName = Properties.Settings.Default.DefaultServiceName;
            int port = Properties.Settings.Default.DefaultPort;

            publishService = new NetService(domain, type, serviceShareName, port);

            publishService.DidPublishService += new NetService.ServicePublished(publishService_DidPublishService);
            publishService.DidNotPublishService += new NetService.ServiceNotPublished(publishService_DidNotPublishService);

            /* HARDCODE TXT RECORD */
            System.Collections.Hashtable dict = new System.Collections.Hashtable();
            //dict.Add("txtvers", "1");

            // add code for combining the host name and the shared folder name.

            dict.Add("PathToService", ShareName);
            publishService.TXTRecordData = NetService.DataFromTXTRecordDictionary(dict);

            publishService.Publish();



        }


        private void StopPublish()
        {
            if (publishService != null)
            {
                publishService.Stop();
            }



        }


        
        private void UpdateSelectedEvent(string selectedEvent)
        {
            
            //handle newly created, or just changes to another folder, and add the menu option for the new event by reloading the list of events.
            LoadThePreviousEvents();

            // if this is a new event how to set the current event.
            //set the current event and the related properties (sharename, images folder, printfolder) to the new event that has been created or selected.
            if (PreviousEventList.Where(z => z.ShareName == selectedEvent).Any())
            {

                CurrentEvent = PreviousEventList.Where(z => z.ShareName == selectedEvent).FirstOrDefault();
            }
            else
            {
                //this must be a new event so don't set the Current Event here.
            }
                        
            SaveUpdatedSettings();
            CreateShares();
            DoPublish();
            
            watcher.ImageUploadedEvent += ImageUploadHandlerForListview;

            
            LoadImagesFromSelectedFolder();
            

        }

        private void LoadThePreviousEvents()
        {
            

            List<ToolStripItem> ToRemoveList = new List<ToolStripItem>();

            var fileItem = (ToolStripMenuItem)menuStrip1.Items["fileToolStripMenuItem"];

            if (fileItem != null)
            {
                //now get the recent menuitem 
                

                var recentMenu = (ToolStripMenuItem) fileItem.DropDownItems["openRecentToolStripMenuItem"];
                if (recentMenu != null)
                {
                    //first loop then remove after to avoid modifying the collection.
                    foreach (ToolStripDropDownItem it in recentMenu.DropDownItems)
                    {
                        ToRemoveList.Add(it);
                    }
                    foreach (ToolStripDropDownItem theItem in ToRemoveList)
                    {

                        recentMenu.DropDownItems.Remove(theItem);
                        theItem.Dispose();
                    }
                    
                    PreviousEventList = ShareMaker.GetPreviousEvents();
                    
                    

                    foreach (ShareMaker.SharedFileModel fm in PreviousEventList)
                    {
                        var item = new ToolStripMenuItem(fm.ShareName, null, RecentMenuItem_Click);
                        item.Name = fm.ShareName;
                       
                        recentMenu.DropDownItems.Add(item);
                    }
                    
                }

                if (isInitialLoad)
                {
                    CurrentEvent = PreviousEventList.FirstOrDefault();
                }
            }
            
         

        }

        private void RecentMenuItem_Click(object sender, EventArgs e)
        {
            UpdateSelectedEvent(sender.ToString()   );
            
        }

       
        private void LoadDefaultValuesForService()
        {

            bool IsPrinterSetForLandscape = Properties.Settings.Default.IsLandscape;

            if (IsPrinterSetForLandscape)
            {
                rdoLandscape.Checked = true;
                rdoPortrait.Checked = false;
            }
            else
            {
                rdoPortrait.Checked = true;
                rdoLandscape.Checked = false;
            }

            LoadThePreviousEvents();

            //default value of 1.
            txtCopies.Text = Properties.Settings.Default.DefaultNumberCopies.ToString();



            txtDefaultToPrint.Text = Properties.Settings.Default.DefaultToPrint.ToString();
            this.TopMargin = Properties.Settings.Default.TopMargin;
            this.LeftMargin = Properties.Settings.Default.LeftMargin;
            this.BottomMargin = Properties.Settings.Default.BottomMargin;
            this.RightMargin = Properties.Settings.Default.RightMargin;

            txtPort.Text = Properties.Settings.Default.DefaultPort.ToString();
            txtServiceName.Text = Properties.Settings.Default.DefaultServiceName;
            txtUsername.Text = Properties.Settings.Default.PrintServerAccount;
            txtPassword.Text = Properties.Settings.Default.PrintServerPassword;
            
            

            txtRight.Text = RightMargin.ToString();
            txtLeft.Text = LeftMargin.ToString();
            txtBottom.Text = BottomMargin.ToString();
            txtTop.Text = TopMargin.ToString();

            

        }

        private void ImageUploadHandlerForListview(Object sender,   EventArgs e)
        {
            this.BeginInvoke(new MethodInvoker(delegate
             {
                 LoadImagesFromSelectedFolder();
             }
             ));
            
        }


        private void SaveUpdatedSettings()
        {
            /*
             * Project Description:  Curator Print Server 
            Project Name -Curator Printer
            Name for the User Account: Curator
            Password for the User Account: Je$$eR0cks
            
            */

            Properties.Settings.Default.LastEventFolder = ShareName;
            Properties.Settings.Default.LastPathToFolder = SharedFolderForImages;
            Properties.Settings.Default.IsLandscape = IsLandscape;
            Properties.Settings.Default.TopMargin = int.Parse(txtTop.Text);
            Properties.Settings.Default.BottomMargin = int.Parse(txtBottom.Text);
            Properties.Settings.Default.LeftMargin = int.Parse(txtLeft.Text);
            Properties.Settings.Default.RightMargin = int.Parse(txtRight.Text);
            Properties.Settings.Default.DefaultToPrint = short.Parse(txtDefaultToPrint.Text);
            Properties.Settings.Default.PrintServerPassword = txtPassword.Text;
            Properties.Settings.Default.PrintServerAccount = txtPassword.Text;
            Properties.Settings.Default.DefaultPort = Int16.Parse( txtPort.Text);
            Properties.Settings.Default.DefaultServiceName = txtServiceName.Text;
            
            Properties.Settings.Default.IsAccountCreated = bool.Parse(txtIsAccountCreated.Text);


            Properties.Settings.Default.Save();

        }



        public void LoadImagesFromSelectedFolder()
        {

            
            listView1.Clear();
            imageList1.Images.Clear();
            imageList2.Images.Clear();


            int thmbNailWidth = 100;
            int thmbNailHeight = 90;
            int largeImageWidth = 200;
            int largeImageHeight = 200;


            ColumnHeader columnHeader1 = new ColumnHeader();
            columnHeader1.Text = "ImageName";
            columnHeader1.TextAlign = HorizontalAlignment.Left;
            columnHeader1.Width = 50;

            ColumnHeader columnHeader2 = new ColumnHeader();
            columnHeader2.Text = "Image";
            columnHeader2.TextAlign = HorizontalAlignment.Center;
            columnHeader2.Width = largeImageWidth;




            imageList1.ImageSize = new Size(thmbNailWidth, thmbNailHeight);
            imageList2.ImageSize = new Size(largeImageWidth, largeImageHeight);

            var allowedExtensions = new[] { ".png", ".gif", ".bmp", ".mpeg4", ".jpeg", "jpg" };

            var files = Directory
                .GetFiles(this.PrintedImagesFolder)
                .Where(file => allowedExtensions.Any(file.ToLower().EndsWith))
                .ToList();

            foreach (string filePath in files)
            {
                string fileName = Path.GetFileName(filePath);
                Image i = PrintingService.SafeImageFromFile (filePath);
                Image img = i.GetThumbnailImage(thmbNailWidth, thmbNailHeight, null, new IntPtr());
                Image img2 = i.GetThumbnailImage(largeImageWidth, largeImageHeight, null, new IntPtr());
                imageList1.Images.Add(img);
                imageList2.Images.Add(img2);

            }

            listView1.LargeImageList = imageList2;
            listView1.SmallImageList = imageList1;

            string fullPath = "";
            for (int j = 0; j < imageList1.Images.Count; j++)

            {
                if (j < files.Count)
                {
                    fullPath = Path.GetFullPath(files[j].ToString());
                }
                ListViewItem lstItem = new ListViewItem();

                lstItem.ImageIndex = j;
                lstItem.Tag = fullPath;
                listView1.Items.Add(lstItem);

            }



        }









        #endregion

        private void PrintManager_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveUpdatedSettings();
        }

        private void showHideSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (grpAdvancedSettings.Visible)
            {
                grpAdvancedSettings.Visible = false;
            }
            else
            {
                grpAdvancedSettings.Visible = true;
            }
        }
    }
}
