﻿namespace PrintServerApp
{
    partial class PrintManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pnlEventName = new System.Windows.Forms.Panel();
            this.lblImageFolder = new System.Windows.Forms.Label();
            this.lblDefaultCopies = new System.Windows.Forms.Label();
            this.txtDefaultToPrint = new System.Windows.Forms.TextBox();
            this.ImageDisplaySplitter = new System.Windows.Forms.Splitter();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.listView1 = new System.Windows.Forms.ListView();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.cboListView = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRight = new System.Windows.Forms.TextBox();
            this.txtBottom = new System.Windows.Forms.TextBox();
            this.txtLeft = new System.Windows.Forms.TextBox();
            this.txtTop = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.rdoLandscape = new System.Windows.Forms.RadioButton();
            this.rdoPortrait = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnPrint = new System.Windows.Forms.Button();
            this.txtCopies = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.browseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.browseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.openRecentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grpAdvancedSettings = new System.Windows.Forms.GroupBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtServiceName = new System.Windows.Forms.TextBox();
            this.showHideSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label16 = new System.Windows.Forms.Label();
            this.txtIsAccountCreated = new System.Windows.Forms.TextBox();
            this.pnlEventName.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpAdvancedSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 16);
            this.label7.TabIndex = 12;
            this.label7.Text = "Image Folder:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(118, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(264, 25);
            this.label9.TabIndex = 7;
            this.label9.Text = "Photobooth Print Server";
            // 
            // pnlEventName
            // 
            this.pnlEventName.Controls.Add(this.lblImageFolder);
            this.pnlEventName.Controls.Add(this.lblDefaultCopies);
            this.pnlEventName.Controls.Add(this.txtDefaultToPrint);
            this.pnlEventName.Controls.Add(this.label7);
            this.pnlEventName.Location = new System.Drawing.Point(12, 40);
            this.pnlEventName.Name = "pnlEventName";
            this.pnlEventName.Size = new System.Drawing.Size(370, 101);
            this.pnlEventName.TabIndex = 17;
            // 
            // lblImageFolder
            // 
            this.lblImageFolder.AutoSize = true;
            this.lblImageFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImageFolder.Location = new System.Drawing.Point(129, 10);
            this.lblImageFolder.Name = "lblImageFolder";
            this.lblImageFolder.Size = new System.Drawing.Size(76, 25);
            this.lblImageFolder.TabIndex = 35;
            this.lblImageFolder.Text = "label3";
            // 
            // lblDefaultCopies
            // 
            this.lblDefaultCopies.AutoSize = true;
            this.lblDefaultCopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDefaultCopies.Location = new System.Drawing.Point(8, 61);
            this.lblDefaultCopies.Name = "lblDefaultCopies";
            this.lblDefaultCopies.Size = new System.Drawing.Size(186, 16);
            this.lblDefaultCopies.TabIndex = 35;
            this.lblDefaultCopies.Text = "# of default copies to print";
            // 
            // txtDefaultToPrint
            // 
            this.txtDefaultToPrint.Location = new System.Drawing.Point(208, 60);
            this.txtDefaultToPrint.Name = "txtDefaultToPrint";
            this.txtDefaultToPrint.Size = new System.Drawing.Size(36, 20);
            this.txtDefaultToPrint.TabIndex = 34;
            this.txtDefaultToPrint.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DefaultCopyQuantityKeypress);
            this.txtDefaultToPrint.Leave += new System.EventHandler(this.CopyQuantityDone);
            // 
            // ImageDisplaySplitter
            // 
            this.ImageDisplaySplitter.Dock = System.Windows.Forms.DockStyle.Right;
            this.ImageDisplaySplitter.Location = new System.Drawing.Point(432, 28);
            this.ImageDisplaySplitter.Name = "ImageDisplaySplitter";
            this.ImageDisplaySplitter.Size = new System.Drawing.Size(837, 665);
            this.ImageDisplaySplitter.TabIndex = 18;
            this.ImageDisplaySplitter.TabStop = false;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(450, 76);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(793, 276);
            this.listView1.TabIndex = 20;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // imageList2
            // 
            this.imageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList2.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // cboListView
            // 
            this.cboListView.FormattingEnabled = true;
            this.cboListView.Location = new System.Drawing.Point(1122, 52);
            this.cboListView.Name = "cboListView";
            this.cboListView.Size = new System.Drawing.Size(121, 21);
            this.cboListView.TabIndex = 21;
            this.cboListView.SelectedIndexChanged += new System.EventHandler(this.cboListView_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1041, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 20);
            this.label1.TabIndex = 22;
            this.label1.Text = "VIEW:";
            // 
            // txtRight
            // 
            this.txtRight.Location = new System.Drawing.Point(229, 105);
            this.txtRight.Name = "txtRight";
            this.txtRight.Size = new System.Drawing.Size(37, 20);
            this.txtRight.TabIndex = 28;
            this.txtRight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RightMarginChanged);
            this.txtRight.Leave += new System.EventHandler(this.RightMarginDone);
            // 
            // txtBottom
            // 
            this.txtBottom.Location = new System.Drawing.Point(72, 105);
            this.txtBottom.Name = "txtBottom";
            this.txtBottom.Size = new System.Drawing.Size(37, 20);
            this.txtBottom.TabIndex = 27;
            this.txtBottom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.BottomMarginChanged);
            this.txtBottom.Leave += new System.EventHandler(this.BottomMarginDone);
            // 
            // txtLeft
            // 
            this.txtLeft.Location = new System.Drawing.Point(231, 70);
            this.txtLeft.Name = "txtLeft";
            this.txtLeft.Size = new System.Drawing.Size(37, 20);
            this.txtLeft.TabIndex = 26;
            this.txtLeft.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.LeftMarginChanged);
            this.txtLeft.Leave += new System.EventHandler(this.LeftMarginDone);
            // 
            // txtTop
            // 
            this.txtTop.Location = new System.Drawing.Point(72, 66);
            this.txtTop.Name = "txtTop";
            this.txtTop.Size = new System.Drawing.Size(37, 20);
            this.txtTop.TabIndex = 25;
            this.txtTop.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TopChanged);
            this.txtTop.Leave += new System.EventHandler(this.TopMarginDone);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(179, 108);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 16);
            this.label14.TabIndex = 24;
            this.label14.Text = "Right:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(10, 108);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 16);
            this.label13.TabIndex = 23;
            this.label13.Text = "Bottom:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(181, 74);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 16);
            this.label12.TabIndex = 22;
            this.label12.Text = "Left:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(10, 69);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 16);
            this.label11.TabIndex = 21;
            this.label11.Text = "Top:";
            // 
            // rdoLandscape
            // 
            this.rdoLandscape.AutoSize = true;
            this.rdoLandscape.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoLandscape.Location = new System.Drawing.Point(263, 38);
            this.rdoLandscape.Name = "rdoLandscape";
            this.rdoLandscape.Size = new System.Drawing.Size(103, 20);
            this.rdoLandscape.TabIndex = 20;
            this.rdoLandscape.TabStop = true;
            this.rdoLandscape.Text = "Landscape";
            this.rdoLandscape.UseVisualStyleBackColor = true;
            this.rdoLandscape.CheckedChanged += new System.EventHandler(this.rdoLandscape_CheckedChanged);
            // 
            // rdoPortrait
            // 
            this.rdoPortrait.AutoSize = true;
            this.rdoPortrait.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoPortrait.Location = new System.Drawing.Point(136, 38);
            this.rdoPortrait.Name = "rdoPortrait";
            this.rdoPortrait.Size = new System.Drawing.Size(76, 20);
            this.rdoPortrait.TabIndex = 19;
            this.rdoPortrait.TabStop = true;
            this.rdoPortrait.Text = "Portrait";
            this.rdoPortrait.UseVisualStyleBackColor = true;
            this.rdoPortrait.CheckedChanged += new System.EventHandler(this.rdoPortrait_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 15);
            this.label10.TabIndex = 18;
            this.label10.Text = "Direction:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtRight);
            this.groupBox2.Controls.Add(this.rdoPortrait);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtBottom);
            this.groupBox2.Controls.Add(this.rdoLandscape);
            this.groupBox2.Controls.Add(this.txtLeft);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.txtTop);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(450, 382);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(363, 140);
            this.groupBox2.TabIndex = 29;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Print Layout";
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(232, 14);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 45);
            this.btnPrint.TabIndex = 31;
            this.btnPrint.Text = "PRINT";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // txtCopies
            // 
            this.txtCopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCopies.Location = new System.Drawing.Point(116, 31);
            this.txtCopies.Name = "txtCopies";
            this.txtCopies.Size = new System.Drawing.Size(54, 26);
            this.txtCopies.TabIndex = 32;
            this.txtCopies.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CopiesKeypress);
            this.txtCopies.Leave += new System.EventHandler(this.CopiesDone);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 16);
            this.label2.TabIndex = 33;
            this.label2.Text = "# of Copies:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1269, 28);
            this.menuStrip1.TabIndex = 34;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.openRecentToolStripMenuItem,
            this.showHideSettingsToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(45, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.browseToolStripMenuItem});
            this.newToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(211, 24);
            this.newToolStripMenuItem.Text = "New";
            // 
            // browseToolStripMenuItem
            // 
            this.browseToolStripMenuItem.Name = "browseToolStripMenuItem";
            this.browseToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.browseToolStripMenuItem.Text = "Browse..";
            this.browseToolStripMenuItem.Click += new System.EventHandler(this.browseToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.browseToolStripMenuItem1});
            this.openToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(211, 24);
            this.openToolStripMenuItem.Text = "Open";
            // 
            // browseToolStripMenuItem1
            // 
            this.browseToolStripMenuItem1.Name = "browseToolStripMenuItem1";
            this.browseToolStripMenuItem1.Size = new System.Drawing.Size(152, 24);
            this.browseToolStripMenuItem1.Text = "Browse";
            this.browseToolStripMenuItem1.Click += new System.EventHandler(this.browseToolStripMenuItem1_Click);
            // 
            // openRecentToolStripMenuItem
            // 
            this.openRecentToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openRecentToolStripMenuItem.Name = "openRecentToolStripMenuItem";
            this.openRecentToolStripMenuItem.Size = new System.Drawing.Size(211, 24);
            this.openRecentToolStripMenuItem.Tag = "OpenRecent";
            this.openRecentToolStripMenuItem.Text = "Open Recent";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnPrint);
            this.groupBox1.Controls.Add(this.txtCopies);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(905, 420);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(338, 71);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Re-Print";
            // 
            // grpAdvancedSettings
            // 
            this.grpAdvancedSettings.Controls.Add(this.txtIsAccountCreated);
            this.grpAdvancedSettings.Controls.Add(this.label16);
            this.grpAdvancedSettings.Controls.Add(this.label8);
            this.grpAdvancedSettings.Controls.Add(this.label15);
            this.grpAdvancedSettings.Controls.Add(this.txtServiceName);
            this.grpAdvancedSettings.Controls.Add(this.label6);
            this.grpAdvancedSettings.Controls.Add(this.label5);
            this.grpAdvancedSettings.Controls.Add(this.label4);
            this.grpAdvancedSettings.Controls.Add(this.txtUsername);
            this.grpAdvancedSettings.Controls.Add(this.txtPassword);
            this.grpAdvancedSettings.Controls.Add(this.txtPort);
            this.grpAdvancedSettings.Location = new System.Drawing.Point(463, 538);
            this.grpAdvancedSettings.Name = "grpAdvancedSettings";
            this.grpAdvancedSettings.Size = new System.Drawing.Size(780, 88);
            this.grpAdvancedSettings.TabIndex = 36;
            this.grpAdvancedSettings.TabStop = false;
            this.grpAdvancedSettings.Text = "Advanced Settings";
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(80, 55);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(95, 20);
            this.txtPort.TabIndex = 2;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(271, 51);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(119, 20);
            this.txtPassword.TabIndex = 3;
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(268, 19);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(122, 20);
            this.txtUsername.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Port";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(213, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Password";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(207, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Username";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(425, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "ServiceName";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(437, 53);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(0, 13);
            this.label15.TabIndex = 11;
            // 
            // txtServiceName
            // 
            this.txtServiceName.Location = new System.Drawing.Point(500, 15);
            this.txtServiceName.Name = "txtServiceName";
            this.txtServiceName.Size = new System.Drawing.Size(153, 20);
            this.txtServiceName.TabIndex = 10;
            // 
            // showHideSettingsToolStripMenuItem
            // 
            this.showHideSettingsToolStripMenuItem.Name = "showHideSettingsToolStripMenuItem";
            this.showHideSettingsToolStripMenuItem.Size = new System.Drawing.Size(211, 24);
            this.showHideSettingsToolStripMenuItem.Text = "Show/Hide Settings";
            this.showHideSettingsToolStripMenuItem.Click += new System.EventHandler(this.showHideSettingsToolStripMenuItem_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 13);
            this.label16.TabIndex = 13;
            this.label16.Text = "IsAccountCreated";
            // 
            // txtIsAccountCreated
            // 
            this.txtIsAccountCreated.Location = new System.Drawing.Point(96, 15);
            this.txtIsAccountCreated.Name = "txtIsAccountCreated";
            this.txtIsAccountCreated.Size = new System.Drawing.Size(79, 20);
            this.txtIsAccountCreated.TabIndex = 14;
            // 
            // PrintManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1269, 693);
            this.Controls.Add(this.grpAdvancedSettings);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboListView);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.ImageDisplaySplitter);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.pnlEventName);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "PrintManager";
            this.Text = "PhotoBooth Print Server";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PrintManager_FormClosing);
            this.Load += new System.EventHandler(this.PrintManager_Load);
            this.pnlEventName.ResumeLayout(false);
            this.pnlEventName.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpAdvancedSettings.ResumeLayout(false);
            this.grpAdvancedSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel pnlEventName;
        private System.Windows.Forms.Splitter ImageDisplaySplitter;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ComboBox cboListView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rdoLandscape;
        private System.Windows.Forms.RadioButton rdoPortrait;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtRight;
        private System.Windows.Forms.TextBox txtBottom;
        private System.Windows.Forms.TextBox txtLeft;
        private System.Windows.Forms.TextBox txtTop;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.TextBox txtCopies;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblDefaultCopies;
        private System.Windows.Forms.TextBox txtDefaultToPrint;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem browseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem browseToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem openRecentToolStripMenuItem;
        private System.Windows.Forms.Label lblImageFolder;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox grpAdvancedSettings;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtServiceName;
        private System.Windows.Forms.ToolStripMenuItem showHideSettingsToolStripMenuItem;
        private System.Windows.Forms.TextBox txtIsAccountCreated;
        private System.Windows.Forms.Label label16;
    }
}

