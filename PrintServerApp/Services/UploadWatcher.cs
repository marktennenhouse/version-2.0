﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace PrintServerApp.Services
{
   public class UploadWatcher
    {
        // properties here.
        public string PrintedImagesFolder { get; set; }
        public string ImageUploadFolder { get; set; }
        public bool IsLandscape { get; set; }
      

        public event EventHandler ImageUploadedEvent;
        private FileSystemWatcher watcher;

        protected virtual void OnImageUploadedEvent(EventArgs e)
        {
            EventHandler handler =  ImageUploadedEvent;
            if (handler != null)
            {
                handler(this, e);
            }

        }




        private void OnError(object source, ErrorEventArgs e)
        {
            string errorText = "";
            if (e.GetException().GetType() == typeof(InternalBufferOverflowException))
            {
                errorText += string.Format("Error: File System Watcher internal buffer overflow at {0}{1}", DateTime.Now, "\r\n");
            }
            else
            {
                errorText += string.Format("Error: Watched directory not accessible at {0} stacktrace: {1}{2}", DateTime.Now, e.GetException().StackTrace , "\r\n");
                
            }

            
            WriteToLogFile(errorText);
        }


        public void Watch(string imageUploads)
        {
            if (watcher != null)
            {
                watcher.EnableRaisingEvents = false;
            }
             watcher = new FileSystemWatcher();
          
            


            ImageUploadFolder = imageUploads;

        
            PrintedImagesFolder = ImageUploadFolder + @"\PrintedImages";

            
            watcher.Path = ImageUploadFolder;
            

            watcher.NotifyFilter = NotifyFilters.Size |
                      NotifyFilters.FileName |
                      NotifyFilters.DirectoryName;
                      
            watcher.Filter = "*.*";
            
            watcher.Created += new FileSystemEventHandler(OnCreateNewImage);
           watcher.Error += new ErrorEventHandler(OnError);


            watcher.EnableRaisingEvents = true;
        }



        static public void WriteToLogFile(Exception ex)
        {
            string outputFile = "logFile.txt";
            //   string pathToFile = string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory, outputFile);
               string pathToFile = string.Format(@"{0}\{1}", Path.GetDirectoryName(Application.ExecutablePath),outputFile);
            string message = string.Format("DateTime:{0}\r\n Message: {1}  \r\n stacktrace:{2} \r\n inner exception:{3}\r\n", DateTime.Now, ex.Message, ex.StackTrace, ex.InnerException == null ? "" : ex.InnerException.ToString()); 
            using (FileStream fs = new FileStream(pathToFile, FileMode.Append, FileSystemRights.AppendData,
            FileShare.Read, 4096, FileOptions.None))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(message);
                    sw.Flush();
                    sw.Close();
                }
            }


        }

        static public void WriteToLogFile(string message)
        {
            string outputFile = "logFile.txt";
            string pathToFile = string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory, outputFile);
           
                using (FileStream fs = new FileStream(pathToFile, FileMode.Append, FileSystemRights.AppendData,
                FileShare.Read, 4096, FileOptions.None))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.WriteLine(message);
                        sw.Flush();
                        sw.Close();
                    }
                }
            
           
        }

        private static bool IsFileReady(String sFilename)
        {
            // If the file can be opened for exclusive access it means that the file
            // is no longer locked by another process.
            try
            {
                using (FileStream inputStream = File.Open(sFilename, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    if (inputStream.Length > 0)
                    {
                        return true;
                    }
                    else
                    { 
                        return false;
                    }

                }
            }
            catch (Exception)
            {
                return false;
            }
        }

       

        private void OnCreateNewImage(object source, FileSystemEventArgs e)
        {

            try
            {



               
                

                string randomFileName = Path.GetRandomFileName();
                string fileExtension = Path.GetExtension(e.Name);
                randomFileName = Path.ChangeExtension(randomFileName, fileExtension);

                //Copies file to another directory.
                if (IsFileReady(e.FullPath))
                {
                    //move to another folder and print.
                    string fileName = Path.GetFileName(e.FullPath);

                    string existingFile = string.Format(@"{0}\{1}", ImageUploadFolder, fileName);
                    PrintingService printingService = new PrintingService();
                    printingService.PrintedImagesFolder = this.PrintedImagesFolder;
                    short numberOfCopies = Properties.Settings.Default.DefaultToPrint;

                    printingService.PrintTheImage(e.FullPath, IsLandscape, numberOfCopies);

                    if (IsFileReady(e.FullPath))
                    {
                        printingService.MoveToCompletedFolder();

                        OnImageUploadedEvent(EventArgs.Empty);



                    }
                }

            }
            catch (Exception ex)
            {

                WriteToLogFile(ex);
            }



        }
    }
}
