﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;

namespace PrintServerApp.Services
{
    public class PrintingService
    {
       
        public string pathToFile { get; set; }
        public string PrintedImagesFolder { get; set; }


        void PrintPageEventOld3(object sender, PrintPageEventArgs e)
        {
            PrintDocument document = (PrintDocument)sender;
            Graphics g = e.Graphics;

            Image image = Image.FromFile(pathToFile);

            int imgWidth = image.Width;
            int imgHeight = image.Height;

            int pageWidth = document.DefaultPageSettings.PaperSize.Width;
            int pageHeight = document.DefaultPageSettings.PaperSize.Height;



            if (imgWidth > pageWidth)
            {
                int diff = imgWidth - pageWidth;
                imgWidth = pageWidth;
                imgHeight -= diff;
            }

            else
            {

                imgWidth = e.MarginBounds.Width;
                imgHeight = e.MarginBounds.Height;


            }

            Rectangle rect = new Rectangle(0, 0, imgWidth, imgHeight);
            g.DrawRectangle(Pens.Purple, rect);
           // g.DrawImage(image, rect);
            image = null;
        }

        

        void PrintPageEvent(object sender, PrintPageEventArgs e)
        {
            PrintDocument document = (PrintDocument)sender;
            Graphics g = e.Graphics;

            Image image = SafeImageFromFile(pathToFile);
            e.PageSettings.Landscape = document.DefaultPageSettings.Landscape;
           Rectangle rectFrame = new Rectangle(e.MarginBounds.Location, new Size(e.MarginBounds.Width - 1, e.MarginBounds.Height - 1));
           g.DrawImage( image, rectFrame);
            image = null;
            
           
        }

        public static Image SafeImageFromFile(string path)
        {
            byte[] bytes = File.ReadAllBytes(path);
            Image img;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                img = Image.FromStream(ms);
                return img;
            }
        }
    

       public  void MoveToCompletedFolder()
        {
           string randomFileName = Path.GetRandomFileName();
            string fileExtension = Path.GetExtension(pathToFile);
            randomFileName = Path.ChangeExtension(randomFileName, fileExtension);
            string AlreadyPrintedImageFolder = string.Format(@"{0}\{1}", PrintedImagesFolder, randomFileName);

            File.Move(pathToFile, AlreadyPrintedImageFolder);
        }

        


        public string PrintTheImage(string pathToFile, bool isLandscape,short numberOfCopies)
        {
            try
            {
                this.pathToFile = pathToFile;
                //PrintDocument document = new PrintDocument();
               

                //PrintController printController = new StandardPrintController();
                //document.OriginAtMargins = true;
                //document.PrinterSettings.Copies = numberOfCopies;
                          
                //document.DefaultPageSettings.Landscape = isLandscape;
                //int leftMargin = Properties.Settings.Default.LeftMargin;
                //int topMargin = Properties.Settings.Default.TopMargin;
                //int rightMargin = Properties.Settings.Default.RightMargin;
                //int bottomMargin = Properties.Settings.Default.BottomMargin;

                //document.DefaultPageSettings.Margins = new Margins(leftMargin, rightMargin, topMargin, bottomMargin);
          
                //document.PrintController = printController;
                //document.PrintPage += new PrintPageEventHandler(PrintPageEvent);


                
               
                   //document.Print();
               
                
                return "DONE";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            
         
        }
        



        }
    }
