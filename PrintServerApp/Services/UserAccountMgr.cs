﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices;


namespace PrintServerApp.Services
{
    class UserAccountMgr
    {
        //http://www.c-sharpcorner.com/article/how-to-create-windows-local-user-account-using-c-sharp/

        public static bool CreateNewUser(string userName, string password)
        {

            try
            {
                DirectoryEntry AD = new DirectoryEntry("WinNT://" +
                                    Environment.MachineName + ",computer");
                
                DirectoryEntry NewUser = AD.Children.Add(userName, "user");
                NewUser.Invoke("SetPassword", new object[] { password });
              //  NewUser.Invoke("Put", new object[] { "Print Server Account", "Print Server Account" });
                NewUser.CommitChanges();
                DirectoryEntry grp;

                grp = AD.Children.Find("Administrators", "group");
                if (grp != null) {
                    grp.Invoke("Add", new object[] { NewUser.Path.ToString() });
                }
                return true;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
        }

    }
}
