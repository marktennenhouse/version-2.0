﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Management;
using System.Security.AccessControl;
using System.Security.Principal;
using System.ComponentModel;

/// <summary>
/// code adapted from https://stackoverflow.com/questions/136539/determining-if-a-folder-is-shared-in-net
/// and https://bytes.com/topic/net/answers/624242-shared-folders
/// </summary>

namespace PrintServerApp.Services
{
  public  class ShareMaker
    {
        private string SetPermissionsOnSharedFolder(string FolderPath, string ShareName, string Description)
        {
            string resultMessage = "OK";
            try
            {
                // Create a ManagementClass object
                ManagementClass managementClass = new ManagementClass("Win32_Share");

                // Create ManagementBaseObjects for in and out parameters
                ManagementBaseObject inParams = managementClass.GetMethodParameters("Create");
                ManagementBaseObject outParams;

                // Set the input parameters
                inParams["Description"] = Description;
                inParams["Name"] = ShareName;
                inParams["Path"] = FolderPath;
                inParams["Type"] = 0x0; // Disk Drive
                //Another Type:
                //        DISK_DRIVE = 0x0
                //        PRINT_QUEUE = 0x1
                //        DEVICE = 0x2
                //        IPC = 0x3
                //        DISK_DRIVE_ADMIN = 0x80000000
                //        PRINT_QUEUE_ADMIN = 0x80000001
                //        DEVICE_ADMIN = 0x80000002
                //        IPC_ADMIN = 0x8000003
                inParams["MaximumAllowed"] = null;
                inParams["Password"] = null;
                inParams["Access"] = null; // Make Everyone has full control access.                
                //inParams["MaximumAllowed"] = int maxConnectionsNum;

                // Invoke the method on the ManagementClass object
                outParams = managementClass.InvokeMethod("Create", inParams, null);
                // Check to see if the method invocation was successful
                if ((uint)(outParams.Properties["ReturnValue"].Value) != 0)
                {
                    //resultMessage = "Unable to share the folder: " + outParams.Properties["ReturnValue"].Value);
                    resultMessage = "Error sharing MMS folders. Please make sure you install as Administrator.";
                    throw new Exception(resultMessage);
                }

                //user selection
                NTAccount ntAccount = new NTAccount("Everyone");

                //SID
                SecurityIdentifier userSID = (SecurityIdentifier)ntAccount.Translate(typeof(SecurityIdentifier));
                byte[] utenteSIDArray = new byte[userSID.BinaryLength];
                userSID.GetBinaryForm(utenteSIDArray, 0);

                //Trustee
                ManagementObject userTrustee = new ManagementClass(new ManagementPath("Win32_Trustee"), null);
                userTrustee["Name"] = "Everyone";
                userTrustee["SID"] = utenteSIDArray;

                //ACE
                ManagementObject userACE = new ManagementClass(new ManagementPath("Win32_Ace"), null);
                userACE["AccessMask"] = 2032127;                                 //Full access
                userACE["AceFlags"] = AceFlags.ObjectInherit | AceFlags.ContainerInherit;
                userACE["AceType"] = AceType.AccessAllowed;
                userACE["Trustee"] = userTrustee;

                ManagementObject userSecurityDescriptor = new ManagementClass(new ManagementPath("Win32_SecurityDescriptor"), null);
                userSecurityDescriptor["ControlFlags"] = 4; //SE_DACL_PRESENT 
                userSecurityDescriptor["DACL"] = new object[] { userACE };

                //can declare share either way, where "ShareName" is the name used to share the folder
                //ManagementPath path = new ManagementPath("Win32_Share.Name='" + ShareName + "'");
                //ManagementObject share = new ManagementObject(path);
                ManagementObject share = new ManagementObject(managementClass.Path + ".Name='" + ShareName + "'");

                share.InvokeMethod("SetShareInfo", new object[] { Int32.MaxValue, Description, userSecurityDescriptor });

                return resultMessage;
            }
            catch (Exception ex)
            {
                throw new Exception(" ERROR: " + ex.Message);
            }
        }



        public static List<SharedFileModel> GetPreviousEvents()
        {
            List<SharedFileModel> sharedFolders = new List<SharedFileModel>();
            List<SharedFileModel> sharedFolders1;
            // Object to query the WMI Win32_Share API for shared files...

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from win32_share");

            ManagementClass mc = new ManagementClass("Win32_Share"); //for local shares

            foreach (ManagementObject share in searcher.Get())
            {

                string type = share["Type"].ToString();

                if (type == "0") // 0 = DiskDrive (1 = Print Queue, 2 = Device, 3 = IPH)
                {
                    string name = share["Name"].ToString(); //getting share name

                    string path = share["Path"].ToString(); //getting share path

                    string caption = share["Caption"].ToString(); //getting share description
                    SharedFileModel sharedFile = new SharedFileModel();
                    sharedFile.ShareName = name;
                    sharedFile.FilePath = path;
                    FileInfo fi = new FileInfo(path);
                    sharedFile.lastAccessed = fi.LastAccessTime;
                    sharedFolders.Add(sharedFile);


                }

            }
            sharedFolders1 = sharedFolders.OrderByDescending(s => s.lastAccessed).ToList();

            return sharedFolders1;
        }



            public SharedFileModel GetSharedFile(string theShareName, string theFolderPath)
            {



                List<SharedFileModel> sharedFolders = GetPreviousEvents();



                if (sharedFolders.Where(z => z.ShareName == theShareName).Any())
                {
                    SharedFileModel sharedFile = sharedFolders.Where(z => z.ShareName == theShareName).First();
                    return sharedFile;
                }
                else
                {
                    return null;
                }

            }


            public string CreateSharedFolder(string folderName, string shareName, string description)
            {
                string finalShareName = string.Empty;

                DirectoryInfo di;
                try
                {


                    DirectoryInfo d1 = new DirectoryInfo(folderName);
                    string printedImagesFolder = string.Format(@"{0}\{1}", folderName, @"\PrintedImages");
                    DirectoryInfo d2 = new DirectoryInfo(printedImagesFolder);

                    //create the folders if they don't exist yet.


                    if (!d1.Exists)
                    {
                        d1.Create();
                    }
                    if (!d2.Exists)
                    {
                        d2.Create();
                    }



                    di = new DirectoryInfo(folderName);

                    SharedFileModel existingFile = GetSharedFile(shareName, folderName );
                    if (existingFile == null)
                    {

                        DirectorySecurity dSecurity = di.GetAccessControl();

                        dSecurity.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
                        di.SetAccessControl(dSecurity);
                        if (di != null)
                        {
                            SetPermissionsOnSharedFolder(folderName, shareName, description);
                            finalShareName = shareName;
                        }
                    }
                    else
                    {

                        //return a value to update the share or event name of the current folder.
                        finalShareName = existingFile.ShareName;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error sharing folders. ERROR: " + ex.Message);
                }

                return finalShareName;

            }



        public class SharedFileModel
        {
            public string FilePath { get; set; }
            public string ShareName { get; set; }
            public DateTime lastAccessed { get; set; }
        }

    }


}




